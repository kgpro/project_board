class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :client
      t.string :executor
      t.string :manager
      t.date :start_date
      t.date :end_date
      t.integer :priority

      t.timestamps null: false
    end
  end
end
