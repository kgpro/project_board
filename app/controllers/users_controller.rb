class UsersController < ApplicationController
  before_action :find_user, only: [:show,:edit,:update,:destroy]
  
  def index
    @users=User.all.order("created_at DESC")
  end
  
  def show
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(users_params)
    
    if @user.save
      redirect_to @user
    else
      render "New"
    end
  end
  
  def edit
  end
  
  def update
    if @user.update(users_params)
      redirect_to @user
    else
      render "edit"
    end
  end
  
  def destroy
    @user.destroy
    redirect_to root_path
  end
  
  private
  
  def users_params
    params.require(:user).permit(:name, :surname, :position, :email)
  end
  
  def find_user
    @user = User.find(params[:id])
  end
end
