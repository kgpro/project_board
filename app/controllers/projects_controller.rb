class ProjectsController < ApplicationController
  before_action :find_project, only: [:show,:edit,:update,:destroy]
  
  def index
    @projects=Project.all.order("created_at DESC")
  end
  
  def show
  end
  
  def new
    @project = Project.new
  end
  
  def create
    @project = Project.new(projects_params)
    
    if @project.save
      redirect_to @project
    else
      render "New"
    end
  end
  
  def edit
  end
  
  def update
    if @project.update(projects_params)
      redirect_to @project
    else
      render "edit"
    end
  end
  
  def destroy
    @project.destroy
    redirect_to root_path
  end
  
  private
  
  def projects_params
    params.require(:project).permit(:title, :client, :executor, :manager, :start_date, :end_date, :priority)
  end
  
  def find_project
    @project = Project.find(params[:id])
  end
end
